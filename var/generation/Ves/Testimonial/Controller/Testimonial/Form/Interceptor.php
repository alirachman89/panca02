<?php
namespace Ves\Testimonial\Controller\Testimonial\Form;

/**
 * Interceptor class for @see \Ves\Testimonial\Controller\Testimonial\Form
 */
class Interceptor extends \Ves\Testimonial\Controller\Testimonial\Form implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Ves\Testimonial\Model\Testimonial $testimonialCollection, \Magento\Framework\Stdlib\DateTime\Timezone $stdTimezone, \Magento\Store\Model\StoreManager $storeManager, \Magento\Framework\Filesystem $filesystem, \Ves\Testimonial\Helper\Data $helper)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $testimonialCollection, $stdTimezone, $storeManager, $filesystem, $helper);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
