<?php
namespace Webkul\SocialSignup\Controller\Twitter\Connect;

/**
 * Interceptor class for @see \Webkul\SocialSignup\Controller\Twitter\Connect
 */
class Interceptor extends \Webkul\SocialSignup\Controller\Twitter\Connect implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Session\Generic $session, \Magento\Framework\App\Action\Context $context, \Magento\Store\Model\Store $store, \Webkul\SocialSignup\Helper\Twitter $helperTwitter, \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute, \Webkul\SocialSignup\Controller\Twitter\TwitterClient $twitterClient, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Customer\Model\Session $customerSession, \Webkul\SocialSignup\Helper\Data $helper, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->___init();
        parent::__construct($session, $context, $store, $helperTwitter, $eavAttribute, $twitterClient, $scopeConfig, $customerSession, $helper, $resultPageFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
