<?php
namespace Webkul\SocialSignup\Controller\Instagram\Connect;

/**
 * Interceptor class for @see \Webkul\SocialSignup\Controller\Instagram\Connect
 */
class Interceptor extends \Webkul\SocialSignup\Controller\Instagram\Connect implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Session\Generic $session, \Magento\Framework\App\Action\Context $context, \Magento\Store\Model\Store $store, \Webkul\SocialSignup\Helper\Instagram $helperInstagram, \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute, \Webkul\SocialSignup\Controller\Instagram\InstagramClient $instagramClient, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Customer\Model\Session $customerSession, \Webkul\SocialSignup\Helper\Data $helper, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->___init();
        parent::__construct($session, $context, $store, $helperInstagram, $eavAttribute, $instagramClient, $scopeConfig, $customerSession, $helper, $resultPageFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
