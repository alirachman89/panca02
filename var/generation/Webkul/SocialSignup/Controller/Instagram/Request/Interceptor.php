<?php
namespace Webkul\SocialSignup\Controller\Instagram\Request;

/**
 * Interceptor class for @see \Webkul\SocialSignup\Controller\Instagram\Request
 */
class Interceptor extends \Webkul\SocialSignup\Controller\Instagram\Request implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Session\Generic $session, \Magento\Framework\App\Action\Context $context, \Webkul\SocialSignup\Controller\Instagram\InstagramClient $instagramClient, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->___init();
        parent::__construct($session, $context, $instagramClient, $resultPageFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
