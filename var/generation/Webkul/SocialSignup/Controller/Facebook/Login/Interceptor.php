<?php
namespace Webkul\SocialSignup\Controller\Facebook\Login;

/**
 * Interceptor class for @see \Webkul\SocialSignup\Controller\Facebook\Login
 */
class Interceptor extends \Webkul\SocialSignup\Controller\Facebook\Login implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Customer\Model\Session $customerSession, \Magento\Customer\Model\ResourceModel\Customer $customerResourceModel, \Magento\Customer\Model\Customer $customerModel, \Webkul\SocialSignup\Model\Facebooksignup $facebooksignupFactory, \Webkul\SocialSignup\Api\FacebooksignupRepositoryInterface $facebooksignupRepository, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\Url\DecoderInterface $urlDecoder, \Magento\Customer\Model\Url $customerUrlModel, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder, \Magento\Framework\HTTP\Client\Curl $curl, \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->___init();
        parent::__construct($context, $customerSession, $customerResourceModel, $customerModel, $facebooksignupFactory, $facebooksignupRepository, $storeManager, $urlDecoder, $customerUrlModel, $scopeConfig, $transportBuilder, $curl, $cookieManager, $resultPageFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
