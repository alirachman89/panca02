<?php
namespace Webkul\SocialSignup\Controller\Google\Connect;

/**
 * Interceptor class for @see \Webkul\SocialSignup\Controller\Google\Connect
 */
class Interceptor extends \Webkul\SocialSignup\Controller\Google\Connect implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Webkul\SocialSignup\Helper\Data $helper, \Magento\Framework\Session\Generic $session, \Magento\Framework\App\Action\Context $context, \Magento\Store\Model\Store $store, \Webkul\SocialSignup\Helper\Google $helperGoogle, \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute, \Webkul\SocialSignup\Controller\Google\GoogleClient $googleClient, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Customer\Model\Session $customerSession, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->___init();
        parent::__construct($helper, $session, $context, $store, $helperGoogle, $eavAttribute, $googleClient, $scopeConfig, $customerSession, $resultPageFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
