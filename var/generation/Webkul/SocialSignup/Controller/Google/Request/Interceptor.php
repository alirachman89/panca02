<?php
namespace Webkul\SocialSignup\Controller\Google\Request;

/**
 * Interceptor class for @see \Webkul\SocialSignup\Controller\Google\Request
 */
class Interceptor extends \Webkul\SocialSignup\Controller\Google\Request implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Session\Generic $session, \Magento\Framework\App\Action\Context $context, \Webkul\SocialSignup\Controller\Google\GoogleClient $googleClient, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->___init();
        parent::__construct($session, $context, $googleClient, $resultPageFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
