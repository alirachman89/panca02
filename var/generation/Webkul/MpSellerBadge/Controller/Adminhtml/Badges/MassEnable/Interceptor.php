<?php
namespace Webkul\MpSellerBadge\Controller\Adminhtml\Badges\MassEnable;

/**
 * Interceptor class for @see \Webkul\MpSellerBadge\Controller\Adminhtml\Badges\MassEnable
 */
class Interceptor extends \Webkul\MpSellerBadge\Controller\Adminhtml\Badges\MassEnable implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Ui\Component\MassAction\Filter $filter, \Webkul\MpSellerBadge\Model\Badge $badge, \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory)
    {
        $this->___init();
        parent::__construct($context, $filter, $badge, $fileUploaderFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
