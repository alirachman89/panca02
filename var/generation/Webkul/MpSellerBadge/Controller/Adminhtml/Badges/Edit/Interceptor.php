<?php
namespace Webkul\MpSellerBadge\Controller\Adminhtml\Badges\Edit;

/**
 * Interceptor class for @see \Webkul\MpSellerBadge\Controller\Adminhtml\Badges\Edit
 */
class Interceptor extends \Webkul\MpSellerBadge\Controller\Adminhtml\Badges\Edit implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $registry, \Webkul\MpSellerBadge\Model\Badge $badge, \Webkul\MpSellerBadge\Api\BadgeRepositoryInterface $badgeRepository, \Webkul\MpSellerBadge\Api\SellerbadgeRepositoryInterface $sellerBadgeRepository, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->___init();
        parent::__construct($context, $registry, $badge, $badgeRepository, $sellerBadgeRepository, $resultPageFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
