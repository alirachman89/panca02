<?php
namespace Webkul\MpSellerBadge\Controller\Adminhtml\Badges\Save;

/**
 * Interceptor class for @see \Webkul\MpSellerBadge\Controller\Adminhtml\Badges\Save
 */
class Interceptor extends \Webkul\MpSellerBadge\Controller\Adminhtml\Badges\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Filesystem $filesystem, \Magento\Backend\App\Action\Context $context, \Webkul\MpSellerBadge\Model\Badge $badge, \Webkul\MpSellerBadge\Api\BadgeRepositoryInterface $badgeRepository, \Magento\Framework\Stdlib\DateTime\DateTime $date, \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory, \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate)
    {
        $this->___init();
        parent::__construct($filesystem, $context, $badge, $badgeRepository, $date, $fileUploaderFactory, $localeDate);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
