<?php
namespace Webkul\MpSellerBadge\Controller\Adminhtml\Badges\MassDisable;

/**
 * Interceptor class for @see \Webkul\MpSellerBadge\Controller\Adminhtml\Badges\MassDisable
 */
class Interceptor extends \Webkul\MpSellerBadge\Controller\Adminhtml\Badges\MassDisable implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Webkul\MpSellerBadge\Model\Badge $badge, \Magento\Ui\Component\MassAction\Filter $filter, \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory)
    {
        $this->___init();
        parent::__construct($context, $badge, $filter, $fileUploaderFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
