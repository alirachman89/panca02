/**
 * @category   Webkul
 * @package    Webkul_MpSellerBadge
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
var config = {
    map: {
        '*': {
            formBadgeWidget: 'Webkul_MpSellerBadge/js/form-badge-widget'
        }
    }
};