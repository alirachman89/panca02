<?php
/**
 * @category   Webkul
 * @package    Webkul_MpSellerBadge
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */ 
namespace Webkul\MpSellerBadge\Controller\Adminhtml\Badges;

use Magento\Backend\App\Action;
use Webkul\MpSellerBadge\Model\Badge;
use Magento\Ui\Component\MassAction\Filter;

class MassEnable extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * object of badge model
     * @var Badge
     */
    protected $_badge;

    /**
     * filter object of Filter
     * @var Filter
     */
    protected $_filter;

    /**
     * @param Action\Context                                   $context
     * @param Filter                                           $filter
     * @param Badge                                            $badge
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     */
    public function __construct(
        Action\Context $context,
        Filter $filter,
        Badge $badge,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
    ) {
        
        $this->_filter = $filter;
        $this->_badge = $badge;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_MpSellerBadge::m_badge');
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $imagesModel = $this->_badge;
        $model = $this->_filter;
        $collection = $model->getCollection($imagesModel->getCollection());
        foreach ($collection as $image) {
            $image->setStatus(1);
            $image->save();
        }
        $this->messageManager->addSuccess(__('Badge(s) enabled successfully.'));
        $resultRedirect = $this->resultRedirectFactory->create();

        return $resultRedirect->setPath('*/*/');
    }
}
