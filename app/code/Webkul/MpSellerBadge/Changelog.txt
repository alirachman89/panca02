Legend

+ Features
- Bugs

------------------------------ Version 2.0.1 --------------------------------------

	+ Compatible with version 2.1.*

-------------------------------- Version 2.0.0 --------------------------------------

	+ Allow the admin to add badges for different sellers.
	+ Admin can add multiple badge.
	+ Buyer can see badges at seller’s profile page.
	+ Admin can delete or change the status of badges.
	+ Admin can manage the seller’s badge.