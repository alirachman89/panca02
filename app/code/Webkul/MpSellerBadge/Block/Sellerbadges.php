<?php
/**
 * @category   Webkul
 * @package    Webkul_MpSellerBadge
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */ 
namespace Webkul\MpSellerBadge\Block;

use Webkul\MpSellerBadge\Api\BadgeRepositoryInterface;
use Webkul\MpSellerBadge\Api\SellerbadgeRepositoryInterface;
use Webkul\Marketplace\Helper\Data;
use Webkul\Marketplace\Model\Seller;

class Sellerbadges extends \Magento\Framework\View\Element\Template
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * badge repository
     * @var badgeRepository
     */
    protected $_badgeRepository;

    /**
     * seller badge repository
     * @var SellerbadgeRepositoryInterface
     */
    protected $_sellerBadgeRepository;

    /**
     * marketplace hleper
     * @var Data
     */
    protected $_helperMarketplace;

    /**
     * seller model of marketplace
     * @var [type]
     */
    protected $_sellerModel;

    /**
     * @param Data                                      $helperMarketplace
     * @param Seller                                    $sellerModel
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Customer\Model\Session           $customerSession
     * @param \Magento\Catalog\Block\Product\Context    $context
     * @param BadgeRepositoryInterface                  $badgeRepository
     * @param SellerbadgeRepositoryInterface            $sellerBadgeRepository
     * @param array                                     $data
     */
    public function __construct(
        Data $helperMarketplace,
        Seller $sellerModel,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Block\Product\Context $context,
        BadgeRepositoryInterface $badgeRepository,
        SellerbadgeRepositoryInterface $sellerBadgeRepository,
        array $data = []
    ) {
        $this->_sellerModel = $sellerModel;
        $this->_helperMarketplace = $helperMarketplace;
        $this->_objectManager = $objectManager;
        $this->_storeManager = $context->getStoreManager();
        $this->_customerSession = $customerSession;
        $this->_badgeRepository = $badgeRepository;
        $this->_sellerBadgeRepository = $sellerBadgeRepository;
        parent::__construct($context, $data);
    }

    /**
     * get loggedin Customer badges
     * @return array
     */
    public function getLoginSellerBadges()
    {
        $customerId = $this->_customerSession->getCustomerId();
        $sellerbadgeCollection = $this->_sellerBadgeRepository->getSellerBadgeCollectionBySellerId($customerId);
        $badgeImage = [];
        foreach ($sellerbadgeCollection as $sellerBadge) {
            $sellerId = $sellerBadge->getSellerId();
            if ($customerId == $sellerId) {
                $badge=  $this->_badgeRepository->getBadgeCollectionById($sellerBadge->getBadgeId());
                $badgeImagePrefix = $this->_storeManager->getStore()->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                );
                if ($badge->getStatus() == 1) {
                    $badgeImage[$badge->getRank()] = $badgeImagePrefix.$badge->getBadgeImageUrl();
                }
            }
        }
        return $badgeImage;

    }

    /**
     * get partner id
     * @return integer
     */
    public function getPartnerId()
    {
        $shopUrl = $this->_helperMarketplace->getProfileUrl();
        if (isset($shopUrl)) {
            $shopUrl=$this->getRequest()->getParam('shop');
        }
        if ($shopUrl) {
            $data=$this->_sellerModel
                    ->getCollection()
                    ->addFieldToFilter(
                        'shop_url',
                        ['eq'=>$shopUrl]
                    );
            foreach ($data as $seller) {
                return $seller->getSellerId();
            }
        }
    }
    
    /**
     * get seller badges
     * @return array
     */
    public function getSellerBadges()
    {
        $customerId = $this->getPartnerId();
        $sellerbadgeCollection = $this->_sellerBadgeRepository->getSellerBadgeCollectionBySellerId($customerId);
        $badgeImage = [];
        foreach ($sellerbadgeCollection as $sellerBadge) {
            $sellerId = $sellerBadge->getSellerId();
            if ($customerId == $sellerId) {
                $badge = $this->_badgeRepository->getBadgeCollectionById($sellerBadge->getBadgeId());
                $badgeImagePrefix = $this->_storeManager->getStore()->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                );
                if ($badge->getStatus() == 1) {
                    $badgeImage[$badge->getRank()] = $badgeImagePrefix.$badge->getBadgeImageUrl();
                }
            }
        }
        return $badgeImage;

    }
}
