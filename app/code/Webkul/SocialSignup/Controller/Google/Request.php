<?php
/**
 * @category   Webkul
 * @package    Webkul_SocialSignup
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */ 
namespace Webkul\SocialSignup\Controller\Google;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Session\Generic;

/**
 * request class of google
 */
class Request extends Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
    /**
     * @var \Magento\Framework\Session\Generic
     */
    protected $_session;
    /**
     * @var googleClient
     */
    protected $_googleClient;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Generic $session
     * @param googleClient
     */
    public function __construct(
        Generic $session,
        Context $context,
        GoogleClient $googleClient,
        PageFactory $resultPageFactory
    ) {
    
        $this->_session = $session;
        $this->_googleClient = $googleClient;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    /**
     * redirect the customer to authentication page
     */
    public function execute()
    {
        $this->_googleClient->setParameters();
        $helper = $this->_objectManager->get('Webkul\SocialSignup\Helper\Data');
        // CSRF protection
        $csrf = hash('sha256', uniqid(rand(), true));
        $this->_session->setGoogleCsrf($csrf);
        $this->_googleClient->setState($csrf);
        if (!($this->_googleClient->isEnabled())) {
            return $helper->redirect404($this);
        }
        $mainwProtocol = $this->getRequest()->getParam('mainw_protocol');
        $this->_session->setIsSecure($mainwProtocol);
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath($this->_googleClient->createRequestUrl());
    }
}
