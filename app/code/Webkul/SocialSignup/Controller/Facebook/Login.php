<?php
/**
 * @category   Webkul
 * @package    Webkul_SocialSignup
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */ 
namespace Webkul\SocialSignup\Controller\Facebook;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
use Webkul\SocialSignup\Api\FacebooksignupRepositoryInterface;
use Magento\Framework\Url\DecoderInterface;
use Magento\Customer\Model\Url;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Area;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Stdlib\CookieManagerInterface;

/*
login class of facebook
 */
class Login extends Action
{
    const REFERER_QUERY_PARAM_NAME = 'referer';
    const XML_PATH_REGISTER_EMAIL_TEMPLATE = 'customer/create_account/email_template';
    const XML_PATH_REGISTER_EMAIL_IDENTITY = 'customer/create_account/email_identity';

    const XML_PATH_CONFIRM_EMAIL_TEMPLATE       = 'customer/create_account/email_confirmation_template';
    const XML_PATH_CONFIRMED_EMAIL_TEMPLATE     = 'customer/create_account/email_confirmed_template';

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var PageFactory
     */
    protected $_customerSession;

    /**
     * Webkul\SocialSignup\Api\FacebooksignupRepositoryInterface
     */
    protected $_facebooksignupRepository;

    /**
     * \Magento\Customer\Model\ResourceModel\Customer
     */
    protected $_customerResourceModel;

    /**
     * \Magento\Customer\Model\Customer
     */
    protected $_customerModel;

    /**
     * Webkul\SocialSignup\Model\facebooksignupFactory
     */
    protected $_facebooksignupFactory;

    /**
     * @var EncoderInterface
     */
    protected $_urlDecoder;

    /**
     * Magento\Customer\Model\Url;
     */
    protected $_customerUrlModel;

    /**
     * @var TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var TransportBuilder
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $_curl;

    /**
     * CookieManager
     *
     * @var CookieManagerInterface
     */
    protected $_cookieManager;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Magento\Customer\Model\Session                    $customerSession
     * @param Magento\Customer\Model\ResourceModel\Customer     $customerResourceModel
     * @param Magento\Customer\Model\Customer                   $customerModel
     * @param Webkul\SocialSignup\Model\Facebooksignup          $facebooksignupFactory
     * @param Magento\Store\Model\StoreManagerInterface         $storeManager
     * @param FacebooksignupRepositoryInterface                 $facebooksignupRepository
     * @param DecoderInterface                                  $urlDecoder
     * @param Url                                               $customerUrlModel
     * @param ScopeConfigInterface                              $scopeConfig
     * @param TransportBuilder                                  $transportBuilder
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\ResourceModel\Customer $customerResourceModel,
        \Magento\Customer\Model\Customer $customerModel,
        \Webkul\SocialSignup\Model\Facebooksignup $facebooksignupFactory,
        FacebooksignupRepositoryInterface $facebooksignupRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        DecoderInterface $urlDecoder,
        Url $customerUrlModel,
        ScopeConfigInterface $scopeConfig,
        TransportBuilder $transportBuilder,
        \Magento\Framework\HTTP\Client\Curl $curl,
        CookieManagerInterface $cookieManager,
        PageFactory $resultPageFactory
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
        $this->_customerUrlModel = $customerUrlModel;
        $this->_urlDecoder = $urlDecoder;
        $this->_facebooksignupFactory = $facebooksignupFactory;
        $this->_customerResourceModel = $customerResourceModel;
        $this->_customerModel = $customerModel;
        $this->_transportBuilder = $transportBuilder;
        $this->_facebooksignupRepository = $facebooksignupRepository;
        $this->_customerSession = $customerSession;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_curl = $curl;
        $this->_cookieManager = $cookieManager;
        parent::__construct($context);
    }

    public function execute()
    {
        $facebookUser = null;
        $customerId = 0;
        $mageCustomer = 0;
        $helper=$this->_objectManager->get('Webkul\SocialSignup\Helper\Data');
        $fbAppId=$helper->getFbAppId();
        $fbAppSecretKey=$helper->getFbSecretKey();
        $cookie = $this->getFacebookCookie($fbAppId, $fbAppSecretKey);
        $facebookUser  = json_decode(
            $this->getFbData('https://graph.facebook.com/v2.6/me?access_token=' . $cookie['access_token'].'&debug=all&fields=id%2Cname%2Cemail%2Cfirst_name%2Clast_name%2Clocale&format=json&method=get&pretty=0&suppress_http_code=1')
        );
        if ($facebookUser!='null') {
            $facebookUser = (array)$facebookUser;
            $session = $this->_customerSession;
            if (!$facebookUser['email']) {
                $session->addError(
                    __(
                        'There is some privacy with this Facebook Account,
                    so please check your account or signup with another account.'
                    )
                );
                $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                return $resultRedirect->setPath('customer/account/login');
            } else {
                $collection = $this->_facebooksignupRepository->getByFbId($facebookUser['id']);
                foreach ($collection as $data) {
                    $customerId = $data['customer_id'];
                    if ($customerId) {
                        $existCustomerChk = $this->_customerModel->load($customerId);
                        if (!$existCustomerChk->getId()) {
                            $this->_facebooksignupRepository->getById($data['entity_id'])->delete();
                            $customerId = 0;
                        }
                    }
                }
                if ($customerId) {
                    $this->messageManager->addSuccess(
                        __(
                            'You have successfully loged in using your facebook account '
                        )
                    );
                        $session->loginById($customerId);
                } else {
                    $customerCollection = $this->_customerModel
                            ->getCollection()
                            ->addFieldToFilter('email', ['eq'=>$facebookUser['email']]);
                    foreach ($customerCollection as $customerData) {
                        $mageCustomer = $customerData->getId();
                    }

                    if ($mageCustomer) {
                        $setcollection = $this->_facebooksignupFactory
                                        ->setCustomerId($mageCustomer)
                                        ->setFbId($facebookUser['id']);
                        $setcollection->save();
                        $this->messageManager->addSuccess(
                            __(
                                'You have successfully logged in using your facebook account'
                            )
                        );
                        $session->loginById($mageCustomer);
                    } else {
                        $this->_registerCustomer($facebookUser, $session);
                    }
                }
                return $this->_loginPostRedirect($session);
            }
        } else {
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath('customer/account/login');
        }
    }

    /**
     * register a customer with facebook details
     * @param  array $data     facebook related data
     * @param  object &$session session data
     */
    private function _registerCustomer($data, &$session)
    {
        $password = hash('sha256', time() . $data['id'] . $data['locale']);
        $customer = $this->_customerModel->setId(null);
        $customer->setData('firstname', $data['first_name']);
        $customer->setData('lastname', $data['last_name']);
        $customer->setData('email', $data['email']);
        $customer->setData('password', $password);
        $customer->setData('is_active', 1);
        $customer->setData('confirmation', null);
        $customer->setConfirmation(null);
        $customer->getGroupId();
        $customer->save();

        $this->_customerModel->load($customer->getId())->setConfirmation(null)->save();
        $customer->setConfirmation(null);
        $session->setCustomerAsLoggedIn($customer);
        $customerId = $session->getCustomerId();

        $type = 'registered';

        $types = [
            'registered'   => self::XML_PATH_REGISTER_EMAIL_TEMPLATE,  // welcome email, when confirmation is disabled
            'confirmed'    => self::XML_PATH_CONFIRMED_EMAIL_TEMPLATE, // welcome email, when confirmation is enabled
            'confirmation' => self::XML_PATH_CONFIRM_EMAIL_TEMPLATE,   // email with confirmation link
        ];

        $this->_sendEmailTemplate(
            $customer,
            $types[$type],
            self::XML_PATH_REGISTER_EMAIL_IDENTITY,
            ['customer' => $customer, 'back_url' => ''],
            0
        );
        $setcollection = $this->_facebooksignupFactory
                            ->setCustomerId($customerId)
                            ->setFbId($data['id']);
        $setcollection->save();
    }

    /**
     * send mail to registered customer
     * @param  object $customer       customer details
     * @param  const $template       template
     * @param  const $sender         sender type
     * @param  integer $storeId        store id
     * @return object
     */
    protected function _sendEmailTemplate(
        $customer,
        $template,
        $sender,
        $templateParams = [],
        $storeId = null
    ) {
        $templateId = $this->_scopeConfig->getValue($template, ScopeInterface::SCOPE_STORE, $storeId);
        $transport = $this->_transportBuilder->setTemplateIdentifier($templateId)
        ->setTemplateOptions(['area' => Area::AREA_FRONTEND, 'store' => $storeId])
        ->setTemplateVars($templateParams)
        ->setFrom($this->_scopeConfig->getValue($sender, ScopeInterface::SCOPE_STORE, $storeId))
        ->addTo($customer->getEmail(), $customer->getName())
        ->getTransport();
        $transport->sendMessage();
        return $customer;
    }

    /**
     * redirct customer to a page
     * @param  object &$session object of session
     * @return string           redirect url
     */
    private function _loginPostRedirect(&$session)
    {
        if ($referer = $this->getRequest()->getParam(self::REFERER_QUERY_PARAM_NAME)) {
            $referer = $this->_urlDecoder->decode($referer);
            if ((strpos($referer, $this->_storeManager->getStore()->getBaseUrl()) === 0)
                    || (
                        strpos(
                            $referer,
                            $this->_storeManager->getStore()->getBaseUrl(
                                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA,
                                true
                            )
                        ) === 0)) {
                            $session->setBeforeAuthUrl($referer);
            } else {
                $session->setBeforeAuthUrl($this->_customerUrlModel->getDashboardUrl());
            }
        } else {
            $session->setBeforeAuthUrl($this->_customerUrlModel->getDashboardUrl());
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath($session->getBeforeAuthUrl(true));
    }

    /**
     * get facebook cookie
     * @param  int $appId     faceboook id
     * @param  string $appSecret facebook secret key
     * @return array
     */
    private function getFacebookCookie($appId, $appSecret)
    {
        $cookieData =  $this->_cookieManager->getCookie('fbsr_' . $appId);
        if ($cookieData != '') {
            return $this->getNewFacebookCookie($appId, $appSecret);
        } else {
            return $this->getOldFacebookCookie($appId, $appSecret);
        }
    }

    /**
     * get old facebook cookie
     * @param  int $appId     faceboook id
     * @param  string $appSecret facebook secret key
     */
    private function getOldFacebookCookie($appId, $appSecret)
    {
        $args = [];
        $cookieData =  $this->_cookieManager->getCookie('fbsr_' . $appId);
        parse_str(trim($cookieData, '\\"'), $args);
        ksort($args);
        $payload = '';
        foreach ($args as $key => $value) {
            if ($key != 'sig') {
                $payload .= $key . '=' . $value;
            }
        }
        $encyptedData = hash('sha256', $payload . $appSecret);
        if ($encyptedData != $args['sig']) {
            return [];
        }
        return $args;
    }

    /**
     * get new facebook cookie
     * @param  int $appId     faceboook id
     * @param  string $appSecret facebook secret key
     */
    private function getNewFacebookCookie($appId, $appSecret)
    {
        $cookieData =  $this->_cookieManager->getCookie('fbsr_' . $appId);
        $signedRequest = $this->parseSignedRequest($cookieData, $appSecret);
        $signedRequest['uid'] = $signedRequest['user_id'];
        if ($signedRequest!='') {
            $accessTokenResponse = $this->getFbData("https://graph.facebook.com/oauth/access_token?client_id=$appId&redirect_uri=&client_secret=$appSecret&code=$signedRequest[code]");
            parse_str($accessTokenResponse);
            $response = json_decode($accessTokenResponse, true);
            $signedRequest['access_token'] = $response['access_token'];
            $signedRequest['expires'] = time() + $response['expires_in'];
        }
        return $signedRequest;
    }

    /**
     * parse the signed request
     * @param  array $signedRequest contain access token & expire date
     * @param  int $secret         secret key
     * @return array
     */
    private function parseSignedRequest($signedRequest, $secret)
    {
        list($encodedSig, $payload) = explode('.', $signedRequest, 2);
        // decode the data
        $sig = $this->base64UrlDecode($encodedSig);
        $data = json_decode($this->base64UrlDecode($payload), true);

        if (strtoupper($data['algorithm']) !== 'HMAC-SHA256') {
            return null;
        }
        // check sig
        $expectedSig = hash_hmac('sha256', $payload, $secret, $raw = true);
        if ($sig !== $expectedSig) {
            return null;
        }
        return $data;
    }

    /**
     * decode the sign
     * @param  string $input
     */
    private function base64UrlDecode($input)
    {
        return base64_decode(strtr($input, '-_', '+/'));
    }
    
    /**
     * get facebook data
     * @param  string $url facebook authentication url
     * @return array
     */
    private function getFbData($url)
    {
        $this->_curl->get($url);
        $response = $this->_curl->getBody();
        return $response;
    }
}
